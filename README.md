### Description
This repository contains 2 applications: one that receives events and one that stores the events. 
The first application (`event-receiver-service`) is a Java Spring Boot application which receives JSON data events via a 
REST API, serializes them into protobuf objects and sends them into a redis queue.
The second application (`event-store-service`) is a simple python application which reads the redis queue,
deserializes them and saves the initial JSON data event into a .txt file.

### How to run

##### Using docker
1. Run docker compose: `docker-compose up --build` (This build all 3 docker images and start the container)
2. Start sending data events: `curl -d '{"event":"a event occured","userId":12, "timestamp":121 }' -H "Content-Type: application/json" -X POST http://localhost:8080/api/v1/events` 
3. See the data events stored in the .txt file inside the `event-store-service`.

##### Without docker (The hard way :) )
1. Start redis server:`redis-server`
2. Start `event-receiver-service`: `./mvnw spring-boot:run` 
3. From `event-store-service` directory:
    a. Create an environment: `python3 -m venv venv`
    b. Enter the created environment: `source ./venv/bin/activate`
    c. Install all packages: `pip install -r requirements.txt`
    d. Start the python app: `python3 main.py`
3. Start sending events: `curl -d '{"event":"a event","userId":12, "timestamp":121 }' -H "Content-Type: application/json" -X POST http://localhost:8080/api/v1/events`
4. See the data events stored in the .txt file inside the `event-store-service`.
