package com.konux.eventreceiverservice.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "app.message-queues.redis", ignoreInvalidFields = true)
public class RedisConfig {
    private boolean enabled;
    private String queue;
    private String host;
    private int port;
}
