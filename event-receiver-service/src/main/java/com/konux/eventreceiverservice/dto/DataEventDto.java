package com.konux.eventreceiverservice.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Builder
@Data
public class DataEventDto {
    @NotNull(message = "UserId must be provided")
    private Integer userId;

    @NotNull(message = "Timestamp must be provided")
    private Long timestamp;

    @NotEmpty(message = "The event must have a name")
    private String event;
}
