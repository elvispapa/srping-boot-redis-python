package com.konux.eventreceiverservice.service;

import com.konux.eventreceiverservice.dto.DataEventDto;
import com.konux.eventreceiverservice.exception.AppException;
import com.konux.eventreceiverservice.messagequeue.MessageQueueService;
import com.konux.eventreceiverservice.serializer.DataEventSerializer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class EventService {

    private final DataEventSerializer dataEventSerializer;
    private final MessageQueueService messageQueueService;

    public void process(DataEventDto eventDto) {
        try {
            final byte[] serializedEvent = dataEventSerializer.serialize(eventDto);
            messageQueueService.sendEvent(serializedEvent);

            log.info("Successfully send data eventDto for userId {}", eventDto.getUserId());
        }
        catch (Exception e) {
            throw new AppException(e.getMessage());
        }
    }
}
