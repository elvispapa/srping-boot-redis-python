package com.konux.eventreceiverservice.messagequeue;

public interface MessageQueueService {
    void sendEvent(byte[] event);
}
