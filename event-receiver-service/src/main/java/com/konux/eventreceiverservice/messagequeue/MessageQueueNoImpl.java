package com.konux.eventreceiverservice.messagequeue;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@ConditionalOnProperty(name = "app.message-queues.enabled", havingValue = "false", matchIfMissing = true)
class MessageQueueNoImpl implements MessageQueueService {

    @Override
    public void sendEvent(byte[] event) {
        throw new UnsupportedOperationException("No message queue client found");
    }
}
