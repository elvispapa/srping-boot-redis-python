package com.konux.eventreceiverservice.messagequeue;

import com.konux.eventreceiverservice.config.RedisConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

@Service
@Slf4j
@ConditionalOnProperty(name = "app.message-queues.redis.enabled", havingValue = "true")
class RedisMessageQueue implements MessageQueueService {

    private final RedisConfig redisConfig;
    private final Jedis jedis;

    @Autowired
    public RedisMessageQueue(RedisConfig redisConfig) {
        this.redisConfig = redisConfig;
        this.jedis = new Jedis(redisConfig.getHost(), redisConfig.getPort());

        log.info("Using RedisMessageQueue...");
    }

    @Override
    public void sendEvent(byte[] event) {
        final String queue = redisConfig.getQueue();
        jedis.lpush(queue.getBytes(), event);
    }
}
