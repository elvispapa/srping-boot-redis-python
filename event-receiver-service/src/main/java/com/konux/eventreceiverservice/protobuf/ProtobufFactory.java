package com.konux.eventreceiverservice.protobuf;


import com.konux.eventreceiverservice.dto.DataEventDto;

public class ProtobufFactory {

	public static ProtobufMessages.DataEvent buildProtoForDataEvent(DataEventDto eventDto) {
		return ProtobufMessages.DataEvent.newBuilder()
				.setEvent(eventDto.getEvent())
				.setUserId(eventDto.getUserId())
				.setTimestamp(eventDto.getTimestamp())
				.build();
	}
}
