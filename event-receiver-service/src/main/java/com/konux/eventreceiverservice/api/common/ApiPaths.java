package com.konux.eventreceiverservice.api.common;

public class ApiPaths {
    public static final String BASE_PATH = "/api/v1";
    public static final String EVENT = "/events";
}
