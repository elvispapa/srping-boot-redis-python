package com.konux.eventreceiverservice.api.event;

import static com.konux.eventreceiverservice.api.common.ApiPaths.BASE_PATH;
import static com.konux.eventreceiverservice.api.common.ApiPaths.EVENT;

import com.konux.eventreceiverservice.dto.DataEventDto;
import com.konux.eventreceiverservice.exception.ApiResponse;
import com.konux.eventreceiverservice.service.EventService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = BASE_PATH, consumes = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class EventController {

    private final EventService eventService;

    @PostMapping(EVENT)
    public ResponseEntity<ApiResponse> process(
            @Valid @RequestBody DataEventDto event
    ) {

        eventService.process(event);

        return ApiResponse.success("Processed");
    }
}
