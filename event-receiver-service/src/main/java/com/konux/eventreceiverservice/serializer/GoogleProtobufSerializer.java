package com.konux.eventreceiverservice.serializer;

import com.konux.eventreceiverservice.dto.DataEventDto;
import com.konux.eventreceiverservice.protobuf.ProtobufMessages;
import com.konux.eventreceiverservice.protobuf.ProtobufFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@ConditionalOnProperty(name = "app.serializers.google-protobuf.enabled", havingValue = "true")
class GoogleProtobufSerializer implements DataEventSerializer {

    public GoogleProtobufSerializer() {
        log.info("Using GoogleProtobufSerializer");
    }

    @Override
    public byte[] serialize(DataEventDto eventDto) {
        ProtobufMessages.DataEvent protoObject = ProtobufFactory.buildProtoForDataEvent(eventDto);
        return protoObject.toByteArray();
    }
}
