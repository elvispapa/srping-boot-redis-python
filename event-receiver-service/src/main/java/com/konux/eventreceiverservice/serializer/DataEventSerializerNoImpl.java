package com.konux.eventreceiverservice.serializer;

import com.konux.eventreceiverservice.dto.DataEventDto;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(name = "app.serializers.google-protobuf.enabled", havingValue = "false")
class DataEventSerializerNoImpl implements DataEventSerializer {

    @Override
    public byte[] serialize(DataEventDto event) {
        throw new UnsupportedOperationException("No data serializer found");
    }
}
