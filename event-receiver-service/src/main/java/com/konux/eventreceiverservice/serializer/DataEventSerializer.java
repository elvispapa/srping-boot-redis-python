package com.konux.eventreceiverservice.serializer;

import com.konux.eventreceiverservice.dto.DataEventDto;

public interface DataEventSerializer {
    byte[] serialize(DataEventDto eventDto);
}
