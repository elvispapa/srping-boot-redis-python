package com.konux.eventreceiverservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventReceiverServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventReceiverServiceApplication.class, args);
    }

}
