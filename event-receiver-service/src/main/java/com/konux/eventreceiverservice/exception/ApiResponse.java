package com.konux.eventreceiverservice.exception;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Builder
@Data
@AllArgsConstructor
@JsonPropertyOrder({ "success", "description" })
public class ApiResponse {

    private final Boolean success;
    private final String description;

    public static ResponseEntity<ApiResponse> error(
            HttpStatus status,
            String errorMsg
    ) {
        return ResponseEntity.status(status.value())
                .body(ApiResponse.builder()
                                  .success(false)
                                  .description(errorMsg)
                                  .build());
    }

    public static ResponseEntity<ApiResponse> success(String description) {
        return ResponseEntity.status(HttpStatus.OK.value())
                             .body(ApiResponse.builder()
                                              .success(true)
                                              .description(description)
                                              .build()
                             );
    }
}
