package com.konux.eventreceiverservice.messagequeue;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.konux.eventreceiverservice.config.RedisConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import redis.clients.jedis.Jedis;

@ExtendWith(MockitoExtension.class)
public class RedisMessageQueueTest {

    @Mock
    private Jedis jedis;

    @Mock
    private RedisConfig redisConfig;

    private RedisMessageQueue redisMessageQueue;

    @BeforeEach
    public void setup(){
        redisMessageQueue = new RedisMessageQueue(redisConfig);
        ReflectionTestUtils.setField(redisMessageQueue, "jedis", jedis);
    }

    @Test
    public void sendEvent() {
        // given
        final byte[] serializedEvent = "Event message".getBytes();

        // and
        when(redisConfig.getQueue()).thenReturn("queueName");
        when(jedis.lpush((byte[]) any(), any())).thenReturn(1L);

        // when
        redisMessageQueue.sendEvent(serializedEvent);

        // then
        verify(jedis).lpush("queueName".getBytes(), serializedEvent);
    }
}
