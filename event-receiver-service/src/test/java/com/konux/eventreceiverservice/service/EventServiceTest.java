package com.konux.eventreceiverservice.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import com.konux.eventreceiverservice.dto.DataEventDto;
import com.konux.eventreceiverservice.exception.AppException;
import com.konux.eventreceiverservice.messagequeue.MessageQueueService;
import com.konux.eventreceiverservice.serializer.DataEventSerializer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.serializer.support.SerializationFailedException;

@ExtendWith(MockitoExtension.class)
public class EventServiceTest {

    @Mock
    private DataEventSerializer dataEventSerializer;

    @Mock
    private MessageQueueService messageQueueService;

    @InjectMocks
    private EventService eventService;

    @Test
    public void processEvent() {
        // given
        DataEventDto eventDto = mock(DataEventDto.class);

        // and
        byte[] serializedEvent = "The serializedEvent".getBytes();
        when(dataEventSerializer.serialize(eventDto)).thenReturn(serializedEvent);

        // and
        doNothing().when(messageQueueService).sendEvent(any());

        // when
        eventService.process(eventDto);

        // then
        verify(dataEventSerializer).serialize(eventDto);
        verify(messageQueueService).sendEvent(serializedEvent);
    }

    @Test
    public void processFailsWhenSerializationFails() {
        // given
        DataEventDto eventDto = mock(DataEventDto.class);

        // and
        doThrow(SerializationFailedException.class).when(dataEventSerializer).serialize(any());

        // expect
        assertThrows(
                AppException.class,
                () -> eventService.process(eventDto)
        );
    }

    @Test
    public void processFailsWhenEventCannotBeSend() {
        // given
        DataEventDto eventDto = mock(DataEventDto.class);

        // and
        byte[] serializedEvent = "The serializedEvent".getBytes();
        when(dataEventSerializer.serialize(eventDto)).thenReturn(serializedEvent);

        doThrow(RuntimeException.class).when(messageQueueService).sendEvent(any());

        // expect
        assertThrows(
                AppException.class,
                () -> eventService.process(eventDto)
        );
    }
}
