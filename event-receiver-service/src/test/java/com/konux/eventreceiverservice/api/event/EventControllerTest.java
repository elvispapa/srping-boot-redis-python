package com.konux.eventreceiverservice.api.event;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.konux.eventreceiverservice.dto.DataEventDto;
import com.konux.eventreceiverservice.exception.AppException;
import com.konux.eventreceiverservice.service.EventService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(EventController.class)
public class EventControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EventService eventService;

    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    public void processEvent() throws Exception {
        // given
        doNothing().when(eventService).process(any());

        // and
        String payload = buildEvent("an event message");

        // when
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/events")
               .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(payload)
        )
        // then
        .andExpect(status().isOk());
    }

    @Test
    public void processEventNotFound() throws Exception {
        // given
        doNothing().when(eventService).process(any());

        // and
        String payload = buildEvent("an event message");

        // when
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/unkown")
                                              .contentType(MediaType.APPLICATION_JSON_VALUE)
                                              .content(payload)
        )
        // then
        .andExpect(status().isNotFound());
    }

    @Test
    public void processEventBadRequest() throws Exception {
        // given
        doNothing().when(eventService).process(any());

        // when payload is missing
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/events")
                                              .contentType(MediaType.APPLICATION_JSON_VALUE)
        )
       // then
       .andExpect(status().isBadRequest());
    }

    @Test
    public void processEventExceptionThrown() throws Exception {
        // given
        doThrow(AppException.class).when(eventService).process(any());

        // and
        String payload = buildEvent("an event message");

        // when
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/events")
                                              .contentType(MediaType.APPLICATION_JSON_VALUE)
                                              .content(payload)
        )
        // then
       .andExpect(status().is5xxServerError());
    }

    private String buildEvent(String eventName) throws JsonProcessingException {
        return mapper.writeValueAsString(
                DataEventDto.builder()
                            .event(eventName)
                            .userId(1234)
                            .timestamp(LocalDate.now().toEpochDay())
                            .build()
        );
    }
}
