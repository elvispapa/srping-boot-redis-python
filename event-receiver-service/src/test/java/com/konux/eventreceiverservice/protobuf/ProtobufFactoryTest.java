package com.konux.eventreceiverservice.protobuf;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.konux.eventreceiverservice.dto.DataEventDto;
import org.junit.jupiter.api.Test;

public class ProtobufFactoryTest {

    @Test
    public void buildProtoForDataEvent() {
        // given
        DataEventDto eventDto = DataEventDto.builder()
                                            .userId(1123)
                                            .event("2 hours of downtime occured")
                                            .timestamp(151869008L)
                                            .build();
        // when
        ProtobufMessages.DataEvent createdProto = ProtobufFactory.buildProtoForDataEvent(eventDto);

        // then
        assertEquals(createdProto.getEvent(), "2 hours of downtime occured");
        assertEquals(createdProto.getTimestamp(), 151869008L);
        assertEquals(createdProto.getUserId(), 1123);
    }
}
