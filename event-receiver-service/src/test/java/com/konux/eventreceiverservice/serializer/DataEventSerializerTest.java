package com.konux.eventreceiverservice.serializer;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.konux.eventreceiverservice.dto.DataEventDto;
import org.junit.jupiter.api.Test;

public class DataEventSerializerTest {

    private final DataEventSerializer dataEventSerializer = new GoogleProtobufSerializer();

    @Test
    public void serialize() {
        // given
        DataEventDto eventDto = DataEventDto.builder()
                                            .userId(1123)
                                            .event("2 hours of downtime occured")
                                            .timestamp(151869008L)
                                            .build();
        // expect
        assertNotNull(dataEventSerializer.serialize(eventDto));
    }

    @Test
    public void serializeExceptionIsThrown() {
        // given
        DataEventDto eventDto = null;

        // expect
        assertThrows(
                Exception.class,
                () -> dataEventSerializer.serialize(eventDto)
        );
    }
}
