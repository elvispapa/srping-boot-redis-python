###About the Project

A Spring Boot application which receives data events, serializes them into google proto buffer objects and send them into a message broker (redis).

The application supports only one REST API endpoint: 
- URL: `http://localhost:8080/api/v1/events`
- Http Method: `POST`
- Payload:  `{ "timestamp": "15186289089", "userId": 1123, "event": "2 hours of downtime occured" }`

The redis configuration can be configured in `application.yaml`.

### How to run the application

1. Make sure redis server is up and running. Open terminal and run `redis-server` to start redis (redis is running on port 6379 by default...change this value in application.yaml)
2. Start the Spring Boot application: `./mvnw spring-boot:run`
3. Start sending events: `curl -d '{"event":"a event","userId":12, "timestamp":121 }' -H "Content-Type: application/json" -X POST http://localhost:8080/api/v1/events` 

##### Authors

* **Elvis Papa** (https://www.elvispapa.com)
