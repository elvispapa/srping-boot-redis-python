REDIS_CONFIG = {
    "host": "localhost",
    # "host": "redis",
    "port": 6379,
    "eventsQueue": "data-events-queue"
}

APP = {
    "eventsOutputFile": "data-events.txt"
}