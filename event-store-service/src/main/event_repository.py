import json

from src.main import app_config
from src.main import protobuf_helper


def save_in_file(event_proto):
    file_name = app_config.APP["eventsOutputFile"]
    event_dict = protobuf_helper.serialize_event_proto_to_dict(event_proto)

    f = open(file_name, 'a')

    with f as outfile:
        json.dump(event_dict, outfile)
        f.write("\n")


def write_in_db(event_proto):
    # read proto and save object in db
    pass


def save(event_proto):
    # write_in_db(event_proto)
    save_in_file(event_proto)
