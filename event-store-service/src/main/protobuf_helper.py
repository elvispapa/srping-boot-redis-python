from google.protobuf.json_format import MessageToDict

from src.protobuf.generated.messages_pb2 import DataEvent


def deserialize_data_event(event_in_bytes) -> DataEvent:
    event_proto = DataEvent()
    event_proto.ParseFromString(event_in_bytes)

    return event_proto


def serialize_event_proto_to_dict(event_proto) -> dict:
    return MessageToDict(event_proto)
