import logging
from src.main import protobuf_helper, event_repository

logging.basicConfig(level=logging.INFO)


# Read messages from the given redis queue and deserialize them to proto and saves them into the disk
class EventWorker:
    def __init__(self, redis_client, events_queue):
        self.redis_client = redis_client
        self.events_queue = events_queue

    def start(self):
        while True:
            try:
                event_bytes = self.redis_client.brpop(self.events_queue)
                event_proto = protobuf_helper.deserialize_data_event(event_bytes[1])

                logging.info("EventWorker received data event for user " + format(event_proto.userId) + "")

                event_repository.save(event_proto)
            except Exception as e:
                logging.error(e)
