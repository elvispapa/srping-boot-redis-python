import unittest

from src.main import protobuf_helper
from src.protobuf.generated.messages_pb2 import DataEvent


def _build_data_event_proto():
    event_proto = DataEvent()
    event_proto.userId = 12
    event_proto.event = "Server went down at 2 am"
    event_proto.timestamp = 12413
    return event_proto


class MyTest(unittest.TestCase):

    def test_deserialize_bytes_to_proto(self):
        # given
        event_proto = _build_data_event_proto()

        # when
        deserialized_event_proto = protobuf_helper.deserialize_data_event(event_proto.SerializeToString())

        # then
        self.assertIsInstance(deserialized_event_proto, DataEvent)
        self.assertEqual(event_proto.userId, deserialized_event_proto.userId)
        self.assertEqual(event_proto.event, deserialized_event_proto.event)
        self.assertEqual(event_proto.timestamp, deserialized_event_proto.timestamp)

    def test_serialize_proto_to_dict(self):
        # given
        event_proto = _build_data_event_proto()

        # when
        proto_dict = protobuf_helper.serialize_event_proto_to_dict(event_proto)

        # then
        self.assertIsInstance(proto_dict, dict)


if __name__ == '__main__':
    unittest.main()