###About the Project

A python application which reads data events from a redis queue as protobuf objects, deserialize them as JSON and then 
store them as in a file in the disk. (.txt file)

### How to run the application

1. Make sure redis is up and running: `redis-server`
2. Create an environment: `python3 -m venv venv`   
2. Enter the created environment: `source ./venv/bin/activate`
3. Install all packages: `pip install -r requirements.txt`
4. Start the python app: `python3 main.py`
5. Start sending data events in protobuf format in the redis queue....and watch the worker processing them...and stores them in a .txt file 

### How to configure the application
The output file can be configured in the module config.py
The same for the redis configuration.

##### Authors

* **Elvis Papa** (https://www.elvispapa.com)
