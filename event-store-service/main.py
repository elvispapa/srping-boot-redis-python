import os
import logging
from redis import Redis 

from src.main import app_config
from src.main.event_worker import EventWorker

logging.basicConfig(level=logging.INFO)

redis_client = Redis(host=app_config.REDIS_CONFIG['host'], port=app_config.REDIS_CONFIG['port'])
#redis_client = Redis(host=os.environ['REDIS_HOST'], port=os.environ['REDIS_PORT'])

if __name__ == '__main__':
    events_queue = app_config.REDIS_CONFIG['eventsQueue']
    worker = EventWorker(redis_client, events_queue)
    worker.start()

    logging.info("Application started")
